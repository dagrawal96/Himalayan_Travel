<?php include('header.php');
      require('AdminLTE/inc/config.php');
?>
<!--Hero Section Start-->
<section class="hero-section">
   <?php include('menu.php')?>
  <div class="container">
    <div class="hero-content" data-animation-effect="fadeInLeftSmall" data-effect-delay="300">
      <!--<h2>Travel & Treks</h2>-->
      <!--<h3>Partner of Nepal</h3>-->
    </div>
    <a class="drop-arrow bounce" href="#trekking"><i class="fa fa-angle-down"></i></a> </div>
</section>
<!--Hero Section End--> 
<!--Package Section Start-->
<section class="package-section" id="trekking">
  <div class="container">
    <div class="booking clearfix" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
      <div class="search-title pull-left">
        <h3>Search Your Trip</h3>
      </div>
      <div class="search_form pull-right">
        <form>
          <select id="destination" class="form-control destination">
          </select>
          <select id="activities" class="form-control activities">
          </select>
          <select id="tripdate" class="form-control tripdate">
          </select>
          <button type="submit" class="btn btn-default">Search</button>
        </form>
      </div>
    </div>
    <div class="title text-center"> 
    <!--<span>FEATURED BEST PACKAGE TREK & TOUR</span>-->
      <h2>Packages</h2>
      <span class="seperator"></span> </div>
    <div class="row" >
    <?php 
      $sql=$mysqli->query("select * from packages limit 4");
      while($SiPackage=$sql->fetch_array()){
          $PackageId=$SiPackage["PackageId"];
          $Title=$SiPackage["Title"];
          $Duration=$SiPackage["Duration"];
          $Photo=$SiPackage["Photo"];          
    ?>
      <div class="col-lg-3 col-md-4" data-animation-effect="fadeInLeftSmall" data-effect-delay="100">
        <div class="box-style effect2">
          <div class="image-wrapper"> <img src="img/<?=$Photo?>" alt="">
          <a href="trekking-detail.php?id=<?=$PackageId?>" class="detail">Trip Detail</a> </div>
          <div class="box-content">
            <h3><a href="#"><?=$Title?></a></h3>
            <p><i class="fa fa-clock-o"></i> Trip Duration: 
            <span class="blue-text"><?=$Duration?></span></p>
          </div>
          <span class="price-tag"><span>All Price Inclusive</span> $500</span> </div>
      </div>
    <?php } ?>     
    </div>
  </div>
</section>
<!--Package Section End--> 

<!--About Section Start-->
<section class="about-section clearfix">
 <!-- <div class="left-block">
    <article>
      <div class="about-title">
        <h2>About <span class="light">Us</span></h2>
      </div>
    </article>
  </div> -->
  <div class="container">
    <div class="right-block">
    <article>
      <!--<div class="text-to-text">
        <h1 class="fade-text text-center">About Us</h1>
        <h3 class="text-bg">Best Places for trekking in Nepal</h3>
      </div>-->
      
      <div class="row padding padding1">
        <div class="col-md-3"> <img src="img/about-img.jpg" alt=""> </div>
        <div class="col-md-9">
          <h4>Who We Are</h4>
      <span class="borer-bottom"></span>
          <p class="lead">We Travel not to escape life,  but for life not  to escape </p>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>
          <hr/>
          <a href="#" class="btn learn-btn">Learn More</a> </div>
      </div>
    </article>
  </div>
  </div>
</section>
<!--About Section End--> 
<!--First Section Start-->
<section class="first-section">
  <div class="container">
    <div id="activities-wrapper" class="activities-wrapper">
      <div class="title text-center"> 
        <!--<span>Leave the road, take  the trails</span>-->
        <h2>Activities</h2>
        <span class="seperator"></span> </div>
      <div class="grid row">
        <div class="grid-item col-md-4" data-animation-effect="fadeInLeftSmall" data-effect-delay="100">
          <article class="act_box"> <img src="img/rafting.jpg" alt="Rafting">
            <div class="act-content">
              <h3> <span class="border"></span> <a href="#">Rafting</a></h3>
            </div>
          </article>
        </div>
        <div class="grid-item col-md-4" data-animation-effect="fadeInUpSmall" data-effect-delay="200">
          <article class="act_box"> <img src="img/bungee-jump.jpg" alt="Bungee Jump">
            <div class="act-content">
              <h3> <span class="border"></span> <a href="#">Bungee Jump</a></h3>
            </div>
          </article>
        </div>
        <div class="grid-item col-md-4" data-animation-effect="fadeInRightSmall" data-effect-delay="300">
          <article class="act_box"> <img src="img/trekking.jpg" alt="Trekking">
            <div class="act-content">
              <h3> <span class="border"></span> <a href="#">Trekking</a></h3>
            </div>
          </article>
        </div>
        <div class="grid-item col-md-4" data-animation-effect="fadeInLeftSmall" data-effect-delay="400">
          <article class="act_box"> <img src="img/paragliding.jpg" alt="Paragliding">
            <div class="act-content">
              <h3> <span class="border"></span> <a href="#">Paragliding</a></h3>
            </div>
          </article>
        </div>
        <div class="grid-item col-md-4" data-animation-effect="fadeInRightSmall" data-effect-delay="500">
          <article class="act_box"> <img src="img/tour.jpg" alt="Ultra Light">
            <div class="act-content">
              <h3> <span class="border"></span> <a href="#">Triangle Tour</a></h3>
            </div>
          </article>
        </div>
      </div>
    </div>
  </div>
</section>
<!--First Section End--> 
<!--Tetimonial Start-->
<section class="testimonial">
  <div class="container">
    <div class="title text-center white-text">
      <h2>Happy Clients</h2>
      <span class="seperator"></span> 
    </div>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
         <?php
        $i=0;
        $html='';
        $latReviews=$mysqli->query("select * from reviews");
        while($SiReviews=$latReviews->fetch_array()){
          if($i==0){
            $class='active';
          }
          else{
            $class='';
          }
       ?>    
       <li data-target="#carouselExampleIndicators" data-slide-to="<?=$i?>" class="<?=$class?>"></li>
      <?php $i=$i+1; } ?> </ol>

      <div class="carousel-inner" role="listbox">
      <?php
        $i=0;
        $html='';
        $latReviews=$mysqli->query("select * from reviews");
        while($SiReviews=$latReviews->fetch_array()){
            $Nickname=$SiReviews["Nickname"];
            $Review=$SiReviews["Review"];
            $Country=$SiReviews["Country"];
            $Date=$SiReviews["Date"]; 
            if($i==0){
               $class='active';
            }  
            else{
              $class='';
            }       
      ?>
      <?=$html?>
      <div class="carousel-item <?=$class?>">
      <div class="testimonial-info">
            <p><?=$Review?> </p>
            <span class="name blue-text"><?=$Nickname?></span>
            <!-- <span class="post">Web Designer</span>  -->
          </div>
        </div>
      <?php $i=$i+1;
       } ?>
      </div>
    </div>
  </div>
</section>
<!--Tetimonial Enda--> 

<?php include('footer.php')?>