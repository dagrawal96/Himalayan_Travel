<?php include('header.php')?>
<!--Hero Section Start-->
<section class="page-top">
  <?php include('menu.php')?>
  <div class="container">
    <div class="page-top-content" data-animation-effect="fadeInLeftSmall" data-effect-delay="300">
      <h2 class="pull-left">Tibet</h2>
      <div class="back-to-home pull-right"><a href="#"><i class="fa fa-home"></i> Back to home</a></div>
    </div>
  </div>
</section>
<!--Hero Section End-->
<section class="inner-content">
  <div class="container">

          
          <p><img src="img/Volunteer-in-Tibet.jpg" alt=""></p>
          <div class="content-box">
            <p>Tibet is Boarder with Nepal and Republic of China. Tibet is known as the highest plateau in the World. Tibet emerged in the 7th century as a unified kingdom, but it soon divided into a variety of territories. The mass of western and central Tibet was often at least nominally unified under a series of Tibetan governments in Lhasa or nearby location, these governments were at many times under Mongol and Chinese over lordship. The eastern regions of Kham and Amdo often tried to maintained a more decentralized indigenous political construction, being divided among a number of small principalities and ethnic groups, while also regularly falling more directly under Chinese rule, most of this area was eventually incorporated into the Chinese provinces of Sichuan and Qinghai. The current borders of Tibet were generally established in the 18th century.</p>
            <p>Following the collapse of the Qing dynasty in 1912, Qing soldiers were disarmed and escorted out of Tibet Area. The region afterward declared its independence in 1913 AD, without acknowledgment by the following Chinese government. Later Lhasa took control of the western part of Xikang Province, China. The region maintained its autonomy until 1951 when, following the attack of Tibet, Tibet became unified into the Peoples Republic of China, and the previous Tibetan government was abolished in 1959 after a failed current uprising. Nowadays, the Peoples Republic of China governs western and central Tibet as the Tibet Autonomous Region, while the eastern areas are now mostly ethnic autonomous prefectures within Sichuan, Qinghai and other neighboring provinces.</p>
            <p>Tibet is situated at an elevation of 3600m, North Everest Base Camp, Sigastse Monastery, Potala palace, Norbulinka palace, Sera Monastery, The pilgrimage sites Mt. Kailash, Namtso Lake and many diverse landscapes are the major highlight of the territory.</p>
           
          </div>
 </div>
</section>

<!--Map Start-->
<!--<section id="map"> </section>-->
<!--Map End-->
<?php include('footer.php')?>