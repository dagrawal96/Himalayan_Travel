<!DOCTYPE html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Travel Himalaya Nepal</title>
<link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700" rel="stylesheet">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
<link href="css/font-awesome.css" rel="stylesheet">
<link href="fonts/weather/css/weather.css" rel="stylesheet">
<link href="css/jquery-ui.css" rel="stylesheet">
<link href="css/select2.min.css" rel="stylesheet">
<link href="css/owl.carousel.min.css" rel="stylesheet">
<link href="css/magnific-popup.css" rel="stylesheet">
<link href="css/animations.css" rel="stylesheet">
<link rel="stylesheet" href="css/style.css" type="text/css">
<link rel="stylesheet" href="css/responsive.css" type="text/css">
</head>
<body>

<!--Preloader Start-->
<div id="preloader">
  <div id="status">&nbsp;</div>
</div>
<!--Preloader End--> 
<!-- scrollToTop --> 
<!-- ================ -->
<div class="scrollToTop"><i class="fa fa-angle-up"></i></div>
<div class="social-link">
  <ul>
    <li><a href="#" class="facebook" data-toggle="tooltip" data-placement="right" title="Facebook"><i class="fa fa-facebook"></i></a></li>
    <li><a href="#" class="twitter" data-toggle="tooltip" data-placement="right" title="Twitter"><i class="fa fa-twitter"></i></a></li>
    <li><a href="#" class="google-plus" data-toggle="tooltip" data-placement="right" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
    <li><a href="#" class="tripadvisor" data-toggle="tooltip" data-placement="right" title="Tripadvisor"><i class="fa fa-tripadvisor"></i></a></li>
    <li><a href="#" class="instagram" data-toggle="tooltip" data-placement="right" title="Instagram"><i class="fa fa-instagram"></i></a></li>
  </ul>
</div>
