<?php require('AdminLTE/inc/config.php');
?>
<header> 
    <!--Top Header Start-->
    <div class="top-header">
      <div class="container clearfix">
        <ul class="top_header_left pull-left hidden-md-down">
          <li>
            <div id="weather"></div>
          </li>
          <li><i class="fa fa-phone"></i>+977-98560-23917, +977-061-463927</li>
          <li><i class="fa fa-email"></i>info@thimalaya.com</li>
          <li><span class="blue-text"><i class="fa fa-clock-o"></i>24 Hour Customer Service</span></li>
        </ul>
        <div class="top_header_right pull-right"> <a class="red-btn btn" href="#" id="ticket_book"><i class="fa fa-ticket"></i>Book For Ticket</a>
          <div id="ticket_form">
            <form>
              <div class="form-group">
                <div class="radio">
                  <input type="radio" name="way" id="one_way" value="one way">
                  <label for="one_way"><span></span>One Way</label>
                </div>
                <div class="radio">
                  <input type="radio" name="way" id="round_trip" value="round trip" checked>
                  <label for="round_trip"><span></span>Round Trip</label>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-6">
                  <label for="fromPlace">From</label>
                  <select id="fromPlace" class="form-control fromplace">
                  </select>
                </div>
                <div class="col-sm-6">
                  <label for="toPlace">To</label>
                  <select id="toPlace" class="form-control toplace">
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-6">
                  <label for="departure">Departure</label>
                  <input type="text" id="departure" class="form-control datepicker"name="departure"  placeholder="mm/dd/yy">
                  <i class="fa fa-calendar"></i> </div>
                <div class="col-sm-6 arrival_date" id="arrival_date">
                  <label for="arrival">Arrival</label>
                  <input type="text" id="arrival" class="form-control datepicker" name="arrival" placeholder="mm/dd/yy">
                  <i class="fa fa-calendar"></i> </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-6">
                  <label for="adult">Adults (12 + Yrs)</label>
                  <select id="adult" class="form-control adult">
                  </select>
                </div>
                <div class="col-sm-6">
                  <label for="child">Child (02-11 Yrs)</label>
                  <select id="child" class="form-control child">
                  </select>
                </div>
              </div>
              <div class="form-group total">
                <label for="total passenger">Total Passenger: 4</label>
              </div>
              <button type="submit" class="btn btn-default">Search Flight <i class="fa fa-paper-plane"></i></button>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!--Top Header End--> 
    <!--Bottom Header Start-->
    <div class="bottom-header" id="bottom-header">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-4 col-xs-12">
            <h1 class="logo"><a href="index.php"><img src="img/logo.png" alt=""><span class="logo-title">Travel Himalaya Nepal</span> <span class="slogan">Your Treks & Tours Partner</span> </a></h1>
          </div>
          <div class="col-lg-9 col-md-8 col-xs-12">
            <div class="main-navigation">
             <div class="search-wrap clearfix hidden-md-down">
                <form class="form-inline text-right">
                  <input class="form-control" type="text" placeholder="SEARCH...">
                  <button class="btn btn-danger my-2 my-sm-0" type="submit">Search</button>
                </form>
              </div>
              <nav class="navbar navbar-toggleable-md navbar-light">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                <a class="navbar-brand" href="#">Menu</a>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav mr-auto">
                    <li class="nav-item active"> <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a> </li>
                    <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">About Us</a>
                      <ul class="dropdown-menu">
                        <li class="nav-item">
                          <a href="about.php" class="dropdown-item">Introduction</a>
                        </li>
                        <li class="nav-item">
                          <a href="our-team.php" class="dropdown-item">Our Team</a>
                        </li>                        
                      </ul>
                    </li>
                    <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Tours</a>
                      <ul class="dropdown-menu">
                        <li class="nav-item"><a href="nepal.php" class="dropdown-item">National Tour </a></li>
                        <li class="nav-item"><a href="india.php" class="dropdown-item">International Tour</a></li>
                        <li class="nav-item"><a href="tibet.php" class="dropdown-item">Tibet</a></li>
                        <li class="nav-item"><a href="bhutan.php" class="dropdown-item">Bhutan</a></li>
                      </ul>
                    </li>
                    <li class="nav-item dropdown"> 
                      <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Trekking packages</a>
                      <ul class="dropdown-menu">
                      <?php
                       $html='';
                       $latCategory=$mysqli->query("select * from `packagecategory`");
                       while($SiCategory=$latCategory->fetch_array()){
                         $CategoryId=$SiCategory["CategoryId"];
                         $Category=$SiCategory["Category"];                         
                         $latPackage=$mysqli->query("select * from packages where PackageCategoryId=$CategoryId");
                          $No_of_packages=$latPackage->num_rows;
                          if($No_of_packages>0){
                            $html.='<li class="nav-item sub-drop">';
                            $html.='<a href="trekking-detail.php?id=4" class="dropdown-item dropdown-toggle" data-toggle="dropdown">';
                            $html.=$Category;
                            $html.='</a>';
                            $html.='<ul class="dropdown-submenu">';
                            while($SiPackage=$latPackage->fetch_array()){
                              $html.='<li><a href="trekking-detail.php?id=';
                              $html.=$SiPackage["PackageId"];
                              $html.='" class="dropdown-item">';
                              $html.=$SiPackage["Title"];
                              $html.='</a></li>';
                            }
                            $html.='</ul>';
                            $html.='</li>';
                          }
                          else{
                             $html.='<li class="nav-item">';
                             $html.='<a href="trekking-detail.php?id=5" class="dropdown-item">';
                             $html.=$Category;
                             $html.='</a>';
                             $html.='</li>';
                          }
                         }
                         ?>
                         
                    <?=$html?>
                  </ul></li>                   
                    
                    <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" data-toggle="dropdown" 
                    href="#">Activities</a>
                      <ul class="dropdown-menu">
                      <?php 
                        $latPackage=$mysqli->query("select * from activities");
                        while($SiPackage=$latPackage->fetch_array()){
                          $ActivityId=$SiPackage["ActivityId"];
                          $Title=$SiPackage["Title"];
                      ?>
                        <li class="nav-item">
                        <a href="activitydetail.php?id=<?=$ActivityId?>" class="dropdown-item"><?=$Title?></a>
                        </li>
                      <?php } ?>
                    </ul>
                    </li>
                    <li class="nav-item"><a href="gallery.php" class="nav-link">Gallery</a></li>
                    <li class="nav-item"><a href="testimonal.php" class="nav-link">Testimonials</a></li>
                    <li class="nav-item"><a href="contact.php" class="nav-link">Contact Us</a></li>
                  </ul>
                </div>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Bottom Header End--> 
  </header>