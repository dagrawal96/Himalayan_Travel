<?php include('header.php');
      require('AdminLTE/inc/config.php');
?>
<!--Hero Section Start-->
<section class="page-top">
  <?php include('menu.php')?>
  <div class="container">
    <div class="page-top-content" data-animation-effect="fadeInLeftSmall" data-effect-delay="300">
      <h2 class="pull-left">Gallery</h2>
      <div class="back-to-home pull-right"><a href="index.php"><i class="fa fa-home"></i> Back to home</a></div>
    </div>
  </div>
</section>
<!--Hero Section End-->
<section class="inner-content">
  <div class="container">          
    <div class="grid row">
    <?php 
      $latPhotos=$mysqli->query("select * from `photos` where PackageId=0");
      while($SiPhotos=$latPhotos->fetch_array()){
        $PhotoId=$SiPhotos["PhotoId"];
        $Photo=$SiPhotos["Imagename"];
    ?>
      <div class="grid-item col-md-4" data-animation-effect="fadeInLeftSmall" data-effect-delay="100">
          <article class="act_box"> <img src="img/<?=$Photo?>" alt="Rafting">            
            <a href="img/<?=$Photo?>" class="overlay popup-img"><i class="fa fa-search-plus"></i></a>
          </article>
      </div>
    <?php } ?>
      <!-- <div class="grid-item col-md-4" data-animation-effect="fadeInUpSmall" data-effect-delay="200">
        <article class="act_box"> <img src="img/bungee-jump.jpg" alt="Bungee Jump">
          <a href="img/bungee-jump.jpg" class="overlay popup-img"><i class="fa fa-search-plus"></i></a>
        </article>
      </div>
      <div class="grid-item col-md-4" data-animation-effect="fadeInRightSmall" data-effect-delay="300">
          <article class="act_box"> <img src="img/trekking.jpg" alt="Trekking">
            <a href="img/trekking.jpg" class="overlay popup-img"><i class="fa fa-search-plus"></i></a>
          </article>
      </div>
      <div class="grid-item col-md-4" data-animation-effect="fadeInLeftSmall" data-effect-delay="400">
        <article class="act_box"> <img src="img/paragliding.jpg" alt="Paragliding">
          <a href="img/paragliding.jpg" class="overlay popup-img"><i class="fa fa-search-plus"></i></a>
        </article>
      </div>
      <div class="grid-item col-md-4" data-animation-effect="fadeInRightSmall" data-effect-delay="500">
          <article class="act_box"> <img src="img/tour.jpg" alt="Ultra Light">
            <a href="img/tour.jpg" class="overlay popup-img"><i class="fa fa-search-plus"></i></a>
          </article>
        </div>
      </div> -->
 </div>
</section>

<!--Map Start-->
<!--<section id="map"> </section>-->
<!--Map End-->
<?php include('footer.php')?>