<?php include('header.php')?>
<!--Hero Section Start-->
<section class="page-top">
  <?php include('menu.php')?>
  <div class="container">
    <div class="page-top-content" data-animation-effect="fadeInLeftSmall" data-effect-delay="300">
      <h2 class="pull-left">About Us</h2>
      <div class="back-to-home pull-right"><a href="index.php"><i class="fa fa-home"></i> Back to home</a></div>
    </div>
  </div>
</section>
<!--Hero Section End-->
<section class="inner-content">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <p><b>Travel Himalaya Nepal</b> Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.</p>
        <p>Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. </p>
      </div>
      <div class="col-md-6"><img src="img/pokhara-1.jpg" alt=""></div>
      
      
      
    </div>
   
      <h3 class="inner-title">Our Mission</h3>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.</p>
        <p>Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. </p>
      
  </div>
</section>

<!--Map Start-->
<!-- <section id="map"> </section> -->
<!--Map End-->
<?php include('footer.php')?>