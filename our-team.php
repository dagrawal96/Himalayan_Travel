<?php include('header.php')?>
<!--Hero Section Start-->
<section class="page-top">
  <?php include('menu.php')?>
  <div class="container">
    <div class="page-top-content" data-animation-effect="fadeInLeftSmall" data-effect-delay="300">
      <h2 class="pull-left">Our Team</h2>
      <div class="back-to-home pull-right"><a href="#"><i class="fa fa-home"></i> Back to home</a></div>
    </div>
  </div>
</section>
<!--Hero Section End-->
<section class="inner-content">
  <div class="container">
    <div class="team">
      <div class="row">
        <div class="col-12 col-md-3 col-sm-3"> <img src="img/trekking.jpg" alt=""> </div>
        <div class="col-12 col-md-9 col-sm-9">
          <div class="team-info">
            <div class="team-title clearfix">
              <h3 class="pull-left">Pritam Sen Thakuri</h3>
              <ul class="team-social pull-right">
                <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#" class="googleplus"><i class="fa fa-google-plus"></i></a></li>
              </ul>
            </div>
            <p>He is a CEO of Webpage Nepal. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu</p>
           <ul class="list">
                        	<li>CEO of Webpage Nepal. </li>
                            <li>CEO of Webpage Nepal. </li>
                            
                            
                        </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="team">
      <div class="row">
        <div class="col-12 col-md-3 col-sm-3"> <img src="img/trekking.jpg" alt=""> </div>
        <div class="col-12 col-md-9 col-sm-9">
          <div class="team-info">
            <div class="team-title clearfix">
              <h3 class="pull-left">Pritam Sen Thakuri</h3>
              <ul class="team-social pull-right">
                <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#" class="googleplus"><i class="fa fa-google-plus"></i></a></li>
              </ul>
            </div>
            <p>He is a CEO of Webpage Nepal. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu</p>
           <ul class="list">
                        	<li>CEO of Webpage Nepal. </li>
                            <li>CEO of Webpage Nepal. </li>
                            
                            
                        </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="team">
      <div class="row">
        <div class="col-12 col-md-3 col-sm-3"> <img src="img/trekking.jpg" alt=""> </div>
        <div class="col-12 col-md-9 col-sm-9">
          <div class="team-info">
            <div class="team-title clearfix">
              <h3 class="pull-left">Pritam Sen Thakuri</h3>
              <ul class="team-social pull-right">
                <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#" class="googleplus"><i class="fa fa-google-plus"></i></a></li>
              </ul>
            </div>
            <p>He is a CEO of Webpage Nepal. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu</p>
           <ul class="list">
                        	<li>CEO of Webpage Nepal. </li>
                            <li>CEO of Webpage Nepal. </li>
                            
                            
                        </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="team">
      <div class="row">
        <div class="col-12 col-md-3 col-sm-3"> <img src="img/trekking.jpg" alt=""> </div>
        <div class="col-12 col-md-9 col-sm-9">
          <div class="team-info">
            <div class="team-title clearfix">
              <h3 class="pull-left">Pritam Sen Thakuri</h3>
              <ul class="team-social pull-right">
                <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#" class="googleplus"><i class="fa fa-google-plus"></i></a></li>
              </ul>
            </div>
            <p>He is a CEO of Webpage Nepal. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qu</p>
           <ul class="list">
                        	<li>CEO of Webpage Nepal. </li>
                            <li>CEO of Webpage Nepal. </li>
                            
                            
                        </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<?php include('footer.php')?>