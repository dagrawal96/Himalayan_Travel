<?php include('header.php');
      require('AdminLTE/inc/config.php');
      $ActivityId=$_GET['id'];
      $latPackage=$mysqli->query("select * from activities where ActivityId=$ActivityId");
      $SiPackage=$latPackage->fetch_array();
      $Activity=$SiPackage["ActivityId"];
	    $Title=$SiPackage["Title"];
	    $Description=$SiPackage["Description"];
      $Photo=$SiPackage["Photo"];
?>
<!--Hero Section Start-->
<section class="page-top">
  <?php include('menu.php')?>
  <div class="container">
    <div class="page-top-content" data-animation-effect="fadeInLeftSmall" data-effect-delay="300">
      <h2 class="pull-left">Activity Detail</h2>
      <div class="back-to-home pull-right"><a href="index.php"><i class="fa fa-home"></i> Back to home</a></div>
    </div>
  </div>
</section>
<!--Hero Section End-->
<section class="inner-content package-section" id="trekking">
  <div class="container"> 
          
         <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner" role="listbox">
    <div class="carousel-item active">
      <img class="d-block img-fluid" src="img/<?=$Photo?>" alt="First slide">
    </div>    
  </div>
  
</div>
       
          <!-- tabs start -->
          <div class="tabs-style"> 
            <!-- Nav tabs -->
            
            <!-- Tab panes -->
            <div class="tab-content">
              <div class="tab-pane fade in active" id="h2tab1">
                <p><?=$Description?></p>                
              </div>            
            </div>
          </div>
          <!-- tabs end --> 
       
  </div>
</section>


<!--Map End-->
<?php include('footer.php')?>