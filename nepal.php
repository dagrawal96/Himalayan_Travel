<?php include('header.php')?>
<!--Hero Section Start-->
<section class="page-top">
  <?php include('menu.php')?>
  <div class="container">
    <div class="page-top-content" data-animation-effect="fadeInLeftSmall" data-effect-delay="300">
      <h2 class="pull-left">Nepal</h2>
      <div class="back-to-home pull-right"><a href="#"><i class="fa fa-home"></i> Back to home</a></div>
    </div>
  </div>
</section>
<!--Hero Section End-->
<section class="inner-content">
  <div class="container">

          
          <p><img src="img/Nepal-2.jpg" alt=""></p>
          <div class="content-box">
            <p>Nepal lies in between India and China (the Autonomous Region of Tibet). Nepal is the home of mighty Himalaya and several different spices of birds, mammals and reptiles. The total population of Nepal recoded to be about 26.62 Million according to the recent survey done my the central Bureau of statistics, Nepal. Nepal has 101 ethnic groups and 92 languages are spoken.</p>
            <p>Nepal is a landlocked country with a total land area of 147,181 sq. km. It lies between 26 22 N - 30 27 N latitude and 80 4E - 88 12E longitude. Roughly rectangular in shape, the land extends approximately 800-kms-east west and 195 kms North to south.The land mass is divided into three geographical zones. Himalayan region, Mid hill region and Terai region. Terai starts from 60m to 305m. Terai is the Grainary store of Nepal. This regions occupies about 17% of the total mass land of Nepal. Mid Hill regions varied from 305m to 1500m and up to 2900m with Mahabharat range long slope terraced. </p>
            <p>The high Himalayan region extends in the north from west to east at an altitude of 4,000 m. to 8,848 m. The world famous peak of Mt. Everest (8848 m.), Kanchanjunga (8586m), Makalu (8463m.), Dhaulagiri (8167m.) Annapurna (8091m.) and many others dominate the formidable range of everlasting snows. The alpine region consists of mountain ranges of Mahabharata varying in height from 1525 m. to 4877m. Below these ranges lies the Churia range at 610 m. to 1524-m. Fertile valleys of various width and altitude lie between these mountain and hill ranges. The southern belt stretches east to west by a width of 26 to 32 km. and a maximum elevation of about 305 metres.</p>
            <p>Several major rivers, tributaries and streams flow southwards originating from the glaciers, snow fed lakes and high Himalayan Mountains of the north. The major rivers are the Mahakali, Seti, Karnali, Gandaki, Koshi and Mechi. The climatic conditions of Nepal vary from one place to another in accordance with the geographical features of Nepal. In the North summers are cools and winter serve while south summers are tropical and winter mild. The Himalayan region always cold and the mountain are cover by snow all around the year.</p>
          </div>
          <div class="category row">
            <div class="col-sm-3"><img src="img/Dzongri.jpg" class="img-thumbnail fill" alt=""></div>
            <div class="col-sm-9 text-area">
              <h4>Nepal Trekking</h4>
              <p> Nepal Trekking has emerged as one of the best trekking destinations in the world. A diverse topography, rich culture and incomparable natural beauty with the warm hospitality of people has made Nepal trekking package, an</p>
              <a href="trekking.php" class="btn btn-primary">read more</a></div>
          </div>
          <div class="category row">
            <div class="col-sm-3"><img src="img/slider5.jpg" class="img-thumbnail fill" alt=""></div>
            <div class="col-sm-9 text-area">
              <h4>Rafting in Nepal</h4>
              <p> Trisuli River Rafting 1/2 days Trisuli River Rafting is the shortest rafting trips in Nepal. It is also one of the most popular because of the scenic Trisuli River. In the course of two days</p>
              <a href="rafting.php" class="btn btn-primary">read more</a></div>
          </div>
          <div class="category row">
            <div class="col-sm-3"><img src="img/jungle-safari-tout-in-nepal.jpg" class="img-thumbnail fill" alt=""></div>
            <div class="col-sm-9 text-area">
              <h4>Jungle Safari</h4>
              <p> Trisuli River Rafting 1/2 days Trisuli River Rafting is the shortest rafting trips in Nepal. It is also one of the most popular because of the scenic Trisuli River. In the course of two days</p>
              <a href="junglesafari.php" class="btn btn-primary">read more</a></div>
          </div>
          
          
          <div class="category row">
            <div class="col-sm-3"><img src="img/sightseeing-nepal.jpg" class="img-thumbnail fill" alt=""></div>
            <div class="col-sm-9 text-area">
              <h4>Sightseeing in Nepal</h4>
              <p> Nepal Motorbiking tour is one of the fantastic adventure package tours for stunning Himalayan views, lush valleys, reach different type of ethnic groups & tribal society, marvelous terrace fields, temples & monasteries. </p>
              <a href="sightseeing.php" class="btn btn-primary">read more</a></div>
          </div>
     
        
    
  </div>
</section>

<!--Map Start-->
<!--<section id="map"> </section>-->
<!--Map End-->
<?php include('footer.php')?>