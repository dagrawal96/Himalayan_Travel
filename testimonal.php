<?php    include('header.php');
         require('AdminLTE/inc/config.php');
  $filename='';
  if(isset($_POST['btnSubmit'])){
  define ("MAX_SIZE","55000");
  function getExtension($str) {
         $i = strrpos($str,".");
         if (!$i) { return ""; }
         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
  } 
  $errors=0;   
  $image = $_FILES["file"]["name"];
  $uploadedfile = $_FILES['file']['tmp_name'];  
  if ($image) 
  {     
      $filename = stripslashes($_FILES['file']['name']);     
      $extension = getExtension($filename);
      $extension = strtolower($extension);  
      if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) 
      {         
          $change='<div class="msgdiv">Unknown Image extension </div> ';
          $errors=1;
      }
      else
      {
          $size=filesize($_FILES['file']['tmp_name']); 
          if ($size > MAX_SIZE*5024)
          {
            $change='<div class="msgdiv">You have exceeded the size limit!</div> ';
            $errors=1;
          }
          if($errors==0){
          $target_file = "img/".strtolower(($_FILES['file']['name']));
          move_uploaded_file($uploadedfile, $target_file);
          $filename=$image;
          }
      }
    }
   $Review= $_POST["txtReview"];
   $Nickname = $_POST['txtNickname'];
   $Country=$_POST["txtCountry"];
   $add_sql = $mysqli->query("INSERT INTO reviews SET Nickname='$Nickname',Review = '$Review',Country='$Country',Photo='$filename',IsPublished=0");
   if($add_sql = TRUE){
       $successMsg = '<div class="alert alert-success">Successfully review Added</div>';
       echo "<meta http-equiv='refresh' content='0'>";
       echo "<script>alert('Thanks for your review.It will be published once reviewed.');
             window.location.href='testimonal.php';
             </script>";
   }else{
       $successMsg = '<div class="alert alert-success">Some Error!!! Contact to Web Page Nepal for IT Help.</div>';
   }
  }
?>
<!--Hero Section Start-->
<section class="page-top">
  <?php include('menu.php')?>
  <div class="container">
    <div class="page-top-content" data-animation-effect="fadeInLeftSmall" data-effect-delay="300">
      <h2 class="pull-left">Testimonial</h2>
      <div class="back-to-home pull-right"><a href="index.php"><i class="fa fa-home"></i> Back to home</a></div>
    </div>
  </div>
</section>
<!--Hero Section End-->
<section class="inner-content">
  <div class="container">          
       <ul class="testimonial-wrap">
       <?php 
                $latReviews=$mysqli->query("select * from reviews");
                while($SiReviews=$latReviews->fetch_array()){
                    $Nickname=$SiReviews["Nickname"];
                    $Review=$SiReviews["Review"];
                    $Country=$SiReviews["Country"];
                    $Date=$SiReviews["Date"];
          
                ?>
                <li> <span class="avtar"><i class="fa fa-user"></i></span>
          <div class="testimonials">
           <span class="name">
            <?=$Nickname?>,<?=$Country?>
            </span> 
            <span class="date">
            <?=$Date?>
            </span>
            <p>
              <?=$Review?>
            </p>
          </div>
        </li>
        <?php } ?>
      </ul>
              <div class="row">
        <div class="col-md-12">
          <div class="review-form clearfix">
            <h2 class="title">Write your own review</h2>
            <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">
              <div class="form-group">
                <label>Nickname<sup>*</sup></label>
                <input type="text" name="txtNickname" id="txtNickname" class="form-control">
              </div>
              <div class="form-group">
                <label>Country<sup>*</sup></label>
                <input type="text" name="txtCountry" id="txtCountry" class="form-control">
              </div>
              <div class="form-group">
                <label>Your Review</label>
                <textarea class="form-control" name="txtReview" id="txtReview" rows="3"></textarea>
              </div>
              <div class="form-group">
                <label>Upload Your Photo</label>
                <input name="file" type="file" class="form-control">
              </div>
			        <div class="form-group">
                <button type="submit" name="btnSubmit" class="btn btn-default">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
 </div>
</section>

<!--Map Start-->
<!-- <section id="map"> </section> -->
<!--Map End-->
<?php include('footer.php')?>