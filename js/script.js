(function ($) {
    $(document).ready(function () {
		$("#ticket_book").click(function(){
		$("#ticket_form").slideToggle("slow");
		});
		
		$('input[name=way]').click(function () {
	
    if (this.id == "one_way") {
        $(".arrival_date").fadeOut('1000');
    } else {
        $(".arrival_date").fadeIn('1000');
    }
});

		
		// makes sure the whole site is loaded

$("#status").fadeOut();
 // will fade out the whole DIV that covers the website.
 $("#preloader").delay(1000).fadeOut("slow");


		$('.navbar .sub-drop').on('click', function(event) {
			// Avoid following the href location when clicking
			//event.preventDefault(); 
			// Avoid having the menu to close when clicking
			event.stopPropagation(); 
			// close all the siblings
			//$(this).removeClass('open');
			//alert($(this).attr('class'));
			// close all the submenus of siblings
			$(this).find('.dropdown-submenu').removeClass('open');
			// opening the one you clicked on
			$(this).toggleClass('open');
			});

		


			$.simpleWeather({
    location: 'Pokhara, Nepal',
    woeid: '',
    unit: 'c',
    success: function(weather) {
      html = '<h2><i class="icon-'+weather.code+'"></i> '+weather.temp+'&deg;'+weather.units.temp+'</h2>';
      html += '<ul><li>'+weather.city+', '+weather.region+'</li>';
      html += '<li class="currently">'+weather.currently+'</li>';
      html += '<li>'+weather.wind.direction+' '+weather.wind.speed+' '+weather.units.speed+'</li></ul>';
  
      $("#weather").html(html);
    },
    error: function(error) {
      $("#weather").html('<p>'+error+'</p>');
    }
  });
  $( ".datepicker" ).datepicker();
var destination = ["Destination", "Nepal", "India", "Bhutan", "Tibet"];
	
	$(".destination").select2({
				  data: destination
				});
	var activities = ["Activities","Trekking", "Rafting", "Paragliding", "Bungee Jump"];		
	$(".activities").select2({
				  data: activities
				});
				var tripdate = ["Trip Duration","1-4 Days", "6-10 Days", "10-14 Days", "10-14 Days"];		
	$(".tripdate").select2({
				  data: tripdate
				});
				var place = ["--Select--", "Bhadrapur", "Bhairahawa", "Bharatpur", "Biratnagar", "Dhangadi", "Janakpur", "Kathmandu", "Nepalgunj", "Pokhara", "Simara", "Tumlingtar"];
	
	$(".fromplace").select2({
				  data: place
				});
	var place2 = ["--Select--","Mountain", "Bhadrapur", "Bhairahawa", "Bharatpur", "Biratnagar", "Dhangadi", "Janakpur", "Kathmandu", "Nepalgunj", "Pokhara", "Simara", "Tumlingtar"];		
	$(".toplace").select2({
				  data: place2
				});
				var number = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];		
	$(".adult").select2({
				  data: number
				});
				$(".child").select2({
				  data: number
				});
				$(".infant").select2({
				  data: number
				});
				$(".no_people").select2({
				  data: number
				});
				 var nationality = ["--Select--","Afghan", "Albanian", "American", "American Samoan", "Andorran", "Angolan", "Anguillan", "Antiguan, Barbudan", "Argentine", "Armenian", "Aruban ", "Australian", "Austrian", "Azerbaijani", "Bahamian", "Bahraini", "Bangladesh", "Barbadian/Bajan", "Basotho", "Belarusian", "Belgian", "Belizean ", "Beninese", "Bermudian", "Bhutanese", "Bolivian", "Bosnian, Herzegovinian", "Brazilian", "British", "British Virgin Islander, Bruneian", "Bulgarian", "Burkinabe", "Burundi ", "Cambodian", "Cameroonian", "Canadian", "Cape Verdean", "Caymanian", "Central African", "Chadian", "Chilean", "Chinese", "Christmas Island", "Cocos Islander"];
	
	$(".nationality").select2({
				  data: nationality
				});
				var currency = ["--Select--","Indian Rupees", "United Arab Emirates Dirham", "British Pound Sterling", "European Union Euro", "Nepali Rupees", "US Dollar"];		
	$(".currency").select2({
				  data: currency
				});
				var packages = ["Select a Package", "Just snap Pokhara", "Flights Close to Fishtail Mountain", "Fly in Between Himalayas", "Air Trek of Mountains"];		
	$(".packages").select2({
				  data: packages
				});	
				$('[data-toggle="tooltip"]').tooltip()
				
		//Owl carousel
		//-----------------------------------------------
		if ($('.owl-carousel').length>0) {
			$(".owl-carousel.carousel").owlCarousel({
				items: 4,
				autoPlay: 5000,
				pagination: false,
				navigation: true,
				navigationText: false
			});
			
			$(".owl-carousel.clients").owlCarousel({
				items: 4,
				autoPlay: true,
				pagination: false,
				itemsDesktopSmall: [992,5],
				itemsTablet: [768,4],
				itemsMobile: [479,3]
			});
			
		};


		 // Magnific popup
        //-----------------------------------------------
        if (($(".popup-img").length > 0) || ($(".popup-iframe").length > 0) || ($(".popup-img-single").length > 0)) {
            $(".popup-img").magnificPopup({
                type: "image",
                gallery: {
                    enabled: true,
                }
            });
            $(".popup-img-single").magnificPopup({
                type: "image",
                gallery: {
                    enabled: false,
                }
            });
            $('.popup-iframe').magnificPopup({
                disableOn: 700,
                type: 'iframe',
                preloader: false,
                fixedContentPos: false
            });
        }
		 // Isotope filters
        //-----------------------------------------------
      
		$('.grid').isotope({
  itemSelector: '.grid-item',
  layoutMode: 'masonry',
  
});

        // Animations
        //-----------------------------------------------
        if (($("[data-animation-effect]").length > 0) && !Modernizr.touch) {
            $("[data-animation-effect]").each(function () {
                var $this = $(this),
                    animationEffect = $this.attr("data-animation-effect");
                if (Modernizr.mq('only all and (min-width: 768px)') && Modernizr.csstransitions) {
                    $this.appear(function () {
                        var delay = ($this.attr("data-effect-delay") ? $this.attr("data-effect-delay") : 1);
                        if (delay > 1) $this.css("effect-delay", delay + "ms");
                        setTimeout(function () {
                            $this.addClass('animated object-visible ' + animationEffect);
                        }, delay);
                    }, { accX: 0, accY: -130 });
                } else {
                    $this.addClass('object-visible');
                }
            });
        };

        //Scroll totop
        //-----------------------------------------------
        $(window).scroll(function () {
			if ($(this).scrollTop() > 1){  
        $('#bottom-header').addClass("fixed-header");
    }
    else{
        $('#bottom-header').removeClass("fixed-header");
    }
            if ($(this).scrollTop() != 0) {
                $(".scrollToTop").fadeIn();
            } else {
                $(".scrollToTop").fadeOut();
            }
        });

        $(".scrollToTop").click(function () {
            $("body,html").animate({ scrollTop: 0 }, 800);
        });
		$('a.drop-arrow').click(function(){
    $('html, body').animate({
        scrollTop: $( $(this).attr('href') ).offset().top-55
    }, 1000);
    return false;
});
 }); // End document ready

})(this.jQuery);



