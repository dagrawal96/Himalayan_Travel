<?php include('header.php')?>
<!--Hero Section Start-->
<section class="page-top">
  <?php include('menu.php')?>
  <div class="container">
    <div class="page-top-content" data-animation-effect="fadeInLeftSmall" data-effect-delay="300">
      <h2 class="pull-left">Bhutan</h2>
      <div class="back-to-home pull-right"><a href="#"><i class="fa fa-home"></i> Back to home</a></div>
    </div>
  </div>
</section>
<!--Hero Section End-->
<section class="inner-content">
  <div class="container">

          
          <p><img src="img/1200px-Taktshang_edit.jpg" alt=""></p>
          <div class="content-box">
            <p>Bhutan is a landlocked country in South Asia. Located in the Eastern Himalayas, it is bordered by the Tibet Autonomous Region of China in the north, India in the south, the Sikkim state of India the Chumbi Valley of Tibet in the west, and the disputed Arunachal Pradesh region in the east. Bhutan is geopolitically in South Asia and is the region's second least populous nation after the Maldives. Thimphu is its capital and largest city, while Phuntsholing is its financial center.</p>
            <p>The independence of Bhutan has endured for centuries, and the territory was never colonized in its history. Situated on the ancient Silk Road between Tibet, the Indian subcontinent and Southeast Asia, the Bhutanese state developed a distinct national identity based on Buddhism. Headed by a spiritual leader known as the Zhabdrung Rinpoche, the territory was composed of many fiefdoms and governed as a Buddhist theocracy. Following a civil war in the 19th century, the House of Wangchuck reunited the country and established relations with the British Empire. Bhutan fostered a strategic partnership with India during the rise of Chinese communism and has a disputed border with the People's Republic of China. In 2008, it transitioned from an absolute monarchy to a constitutional monarchy and held the first election to the National Assembly of Bhutan, that has a two party system characterizing Bhutanese democracy.</p>
            <p>The King of Bhutan is known as the "Dragon King". Bhutan is also notable for pioneering the concept of gross national happiness. The country's landscape ranges from lush subtropical plains in the south to the sub-alpine Himalayan mountains in the north, where there are peaks in excess of 7,000 metres (23,000 ft). The highest mountain in Bhutan is the Gangkhar Puensum, which is also a strong candidate for the highest unclimbed mountain in the world. There is also diverse wildlife in Bhutan.</p>
           
          </div>
 </div>
</section>

<!--Map Start-->
<!--<section id="map"> </section>-->
<!--Map End-->
<?php include('footer.php')?>