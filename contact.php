<?php include('header.php')?>
<!--Hero Section Start-->
<section class="page-top">
  <?php include('menu.php')?>
  <div class="container">
    <div class="page-top-content" data-animation-effect="fadeInLeftSmall" data-effect-delay="300">
      <h2 class="pull-left">Contact Us</h2>
      <div class="back-to-home pull-right"><a href="index.php"><i class="fa fa-home"></i> Back to home</a></div>
    </div>
  </div>
</section>
<!--Hero Section End-->
<section class="inner-content">
  <div class="container">
	<div class="contact-info">
        <div class="row">
          <div class="col-12 col-md-7 col-sm-6">
            <h3>Contact Information</h3>
            <ul>
              <li><i class="fa fa-map-marker"></i> Lakeside-06, Pokhara, Nepal</li>
              <li><i class="fa fa-mobile"></i>+977-061-463927, +977-9856023917</li>
              <li><i class="fa fa-envelope"></i> t.himalayanepal@gmail.com</li>
              <li><i class="fa fa-globe"></i> www.thimalaya.com </li>
            </ul>
			
          </div>
          <div class="col-12 col-md-5 col-sm-6">
            <h3>Get In Touch</h3>
            <p class="lead">Please fill up the required(<span class="red-text">*</span>) field</p>
            <form class="contact-form">
              <div class="form-group">
                <input type="text" class="form-control" id="exampleInputName" placeholder="Fullname *" required="">
              </div>
              <div class="form-group">
                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email Address *" required="">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" id="exampleInputPhoneNumber" placeholder="Your phone number">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" id="exampleInputSubject" placeholder="Subject">
              </div>
              <div class="form-group">
                <textarea class="form-control" id="exampleTextarea" rows="3" placeholder="Message *" required=""></textarea>
              </div>
              
              
             
              <button type="submit" class="btn btn-outline">Submit</button>
            </form>
          </div>
        </div>
      </div>            
 </div>
</section>

<!--Map Start-->
<!--<section id="map"> </section>-->
<!--Map End-->
<?php include('footer.php')?>