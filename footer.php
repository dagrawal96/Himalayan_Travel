<!--Footer Start-->
<footer>
  <!--<div class="newsletter">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-xs-12"> <b>Sign Up For our newsletter</b>
          <form>
            <div class="form-group">
              <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Your email id...">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
        </div>
        <div class="col-md-4 col-xs-12">
          <ul class="footer-social text-right">
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
            <li><a href="#"><i class="fa fa-tripadvisor"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div> -->
  <div class="container">
    <div class="middle-footer">
      <div class="row">
        <div class="col-md-3">
          <h3>About Company</h3>
          <ul>
            <li><a href="#">Our Team</a></li>
            <li><a href="#">Testimonial</a></li>
            <li><a href="#">Legal Documents</a></li>
            <li><a href="#">Responsibility</a></li>
          </ul>
        </div>
        <div class="col-md-3">
          <h3>Destinations</h3>
          <ul>
            <li><a href="#">Nepal</a></li>
            <li><a href="#">India</a></li>
            <li><a href="#">Bhutan</a></li>
            <li><a href="#">Tibet</a></li>
          </ul>
        </div>
        <div class="col-md-3">
          <h3>Other Activities</h3>
          <ul>
            <li><a href="#">Nepal Trekking</a></li>
            <li><a href="#">Rafting in Nepal</a></li>
            <li><a href="#">Nepal Hiking</a></li>
            <li><a href="#">Triangle Tour</a></li>
          </ul>
        </div>
        <div class="col-md-3">
          <h3>Contact Info</h3>
          <ul>
            <li>Travel Himalaya Nepal Pvt.Ltd.</li>
            <li>Lakeside-6, Pokhara, Nepal</li>
            <li>Phone: 977 061 463927</li>
            <li>Mobile: 977 9856023917</li>
            <li>Email: thimalayanepal@gmail.com</li>
          </ul>
        </div>
      </div>
    </div>
    <div class="associate">
      <div class="row">
        <div class="col-md-5">
          <h3>We are associated with:</h3>
            <ul>
              <li><a href="#" data-toggle="tooltip" data-placement="top" title="NMA"><img src="img/logo-nma.png" alt=""></a></li>
              <li><a href="#" data-toggle="tooltip" data-placement="top" title="TAAN"><img src="img/asso4.png" alt=""></a></li>
              <li><a href="#" data-toggle="tooltip" data-placement="top" title="TAAN"><img src="img/Emblem_of_Nepal.svg.png" alt=""></a></li>
              <li><a href="#" data-toggle="tooltip" data-placement="top" title="PATA"><img src="img/pata.png" alt=""></a></li>
              <li><a href="#" data-toggle="tooltip" data-placement="top" title="KEEP"><img src="img/keep.png" alt=""></a></li>
            </ul>
        </div>
        <div class="col-md-3">
          <h3>We Accept:</h3>
            <ul>
              <li><a href="#" data-toggle="tooltip" data-placement="top" title="visa"><img src="img/visa.png" alt=""></a></li>
              <li><a href="#" data-toggle="tooltip" data-placement="top" title="master"><img src="img/mastercard.png" alt=""></a></li>
            </ul>
        </div>
        <div class="col-md-4">
          <!--Map Start-->
        <section id="map"> </section>
      <!--Map End--> 
        </div>
      </div>
    </div>
    <div class="bottom-footer clearfix"> <small>© 2017 Travel Himalaya Nepal. All Right Reserved.</small> <small>Designed By: <a href="#">Webpage Nepal</a></small> </div>
  </div>
  </div>
</footer>
<!--Footer End--> 
<!-- jQuery first, then Tether, then Bootstrap JS. --> 
<script src="js/jquery-3.1.1.min.js" type="text/javascript"></script> 
<script src="js/tether.js" type="text/javascript"></script> 
<script src="js/bootstrap.min.js" type="text/javascript"></script> 
<script src="js/modernizr.js" type="text/javascript"></script> 
<script src="js/jquery.appear.js" type="text/javascript"></script> 
<script src="js/jquery.simpleWeather.js" type="text/javascript"></script> 
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/select2.min.js"></script> 
<script type="text/javascript" src="js/isotope.pkgd.min.js"></script> 
<script type="text/javascript" src="js/owl.carousel.js"></script> 
<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGe_F0BQzzAueb0B4ZAUTWPv3Nejp0a7o&callback=initMap"
    async defer></script> 
<script type="text/javascript" src="js/googlemap.js"></script> 
<script src="js/snap.svg-min.js"></script> 
<script src="js/client_captcha.js" defer></script>
<script src="js/script.js" type="text/javascript"></script>
<script>
    document.addEventListener("DOMContentLoaded", function() {
        document.body.scrollTop; //force css repaint to ensure cssom is ready

        var timeout; //global timout variable that holds reference to timer

        var captcha = new $.Captcha({
            onFailure: function() {

                $(".captcha-chat .wrong").show({
                    duration: 30,
                    done: function() {
                        var that = this;
                        clearTimeout(timeout);
                        $(this).removeClass("shake");
                        $(this).css("animation");
                        //Browser Reflow(repaint?): hacky way to ensure removal of css properties after removeclass
                        $(this).addClass("shake");
                        var time = parseFloat($(this).css("animation-duration")) * 1000;
                        timeout = setTimeout(function() {
                            $(that).removeClass("shake");
                        }, time);
                    }
                });

            },

            onSuccess: function() {
                alert("CORRECT!!!");
            }
        });

        captcha.generate();
    });
    </script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/59785f960d1bb37f1f7a5e56/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>
</html>