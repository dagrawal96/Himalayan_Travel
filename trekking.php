<?php include('header.php')?>
<!--Hero Section Start-->
<section class="page-top">
  <?php include('menu.php')?>
  <div class="container">
    <div class="page-top-content" data-animation-effect="fadeInLeftSmall" data-effect-delay="300">
      <h2 class="pull-left">Trekking</h2>
      <div class="back-to-home pull-right"><a href="index.php"><i class="fa fa-home"></i> Back to home</a></div>
    </div>
  </div>
</section>
<!--Hero Section End-->
<section class="inner-content package-section" id="trekking">
  <div class="container">
    <div class="title text-center"> 
      <h2>Trekking Packages</h2>
      <span class="seperator"></span> </div>
    <div class="row" >
      <div class="col-md-3" data-animation-effect="fadeInLeftSmall" data-effect-delay="100">
        <div class="box-style effect2">
          <div class="image-wrapper"> <img src="img/DSE_3486.jpg" alt=""> <a href="#" class="detail">Trip Detail</a> </div>
          <div class="box-content">
            <h3><a href="#">Annapurna Base Camp</a></h3>
            <p><i class="fa fa-clock-o"></i> Trip Duration: <span class="blue-text">18 Day(s)</span></p>
          </div>
          <span class="price-tag"><span>All Price Inclusive</span> $500</span> </div>
      </div>
      <div class="col-md-3" data-animation-effect="fadeInLeftSmall" data-effect-delay="200">
        <div class="box-style effect2">
          <div class="image-wrapper"> <img src="img/DSE_3486.jpg" alt=""> <a href="#" class="detail">Trip Detail</a> </div>
          <div class="box-content">
            <h3><a href="#">Annapurna Base Camp</a></h3>
            <p><i class="fa fa-clock-o"></i> Trip Duration: <span class="blue-text">18 Day(s)</span></p>
          </div>
          <span class="price-tag blue-bg"><span>All Price Inclusive</span> $500</span> </div>
      </div>
      <div class="col-md-3" data-animation-effect="fadeInLeftSmall" data-effect-delay="300">
        <div class="box-style effect2">
          <div class="image-wrapper"> <img src="img/DSE_3486.jpg" alt=""> <a href="#" class="detail">Trip Detail</a></div>
          <div class="box-content">
            <h3><a href="#">Annapurna Base Camp</a></h3>
            <p><i class="fa fa-clock-o"></i> Trip Duration: <span class="blue-text">18 Day(s)</span></p>
          </div>
          <span class="price-tag orange-bg"><span>All Price Inclusive</span> $500</span> </div>
      </div>
      <div class="col-md-3" data-animation-effect="fadeInLeftSmall" data-effect-delay="400">
        <div class="box-style effect2">
          <div class="image-wrapper"> <img src="img/DSE_3486.jpg" alt=""> <a href="#" class="detail">Trip Detail</a></div>
          <div class="box-content">
            <h3><a href="#">Annapurna Base Camp</a></h3>
            <p><i class="fa fa-clock-o"></i> Trip Duration: <span class="blue-text">18 Day(s)</span></p>
          </div>
          <span class="price-tag green-bg"><span>All Price Inclusive</span> $500</span> </div>
      </div>
      <div class="col-md-3" data-animation-effect="fadeInLeftSmall" data-effect-delay="100">
        <div class="box-style effect2">
          <div class="image-wrapper"> <img src="img/DSE_3486.jpg" alt=""> <a href="#" class="detail">Trip Detail</a> </div>
          <div class="box-content">
            <h3><a href="#">Annapurna Base Camp</a></h3>
            <p><i class="fa fa-clock-o"></i> Trip Duration: <span class="blue-text">18 Day(s)</span></p>
          </div>
          <span class="price-tag"><span>All Price Inclusive</span> $500</span> </div>
      </div>
      <div class="col-md-3" data-animation-effect="fadeInLeftSmall" data-effect-delay="200">
        <div class="box-style effect2">
          <div class="image-wrapper"> <img src="img/DSE_3486.jpg" alt=""> <a href="#" class="detail">Trip Detail</a> </div>
          <div class="box-content">
            <h3><a href="#">Annapurna Base Camp</a></h3>
            <p><i class="fa fa-clock-o"></i> Trip Duration: <span class="blue-text">18 Day(s)</span></p>
          </div>
          <span class="price-tag blue-bg"><span>All Price Inclusive</span> $500</span> </div>
      </div>
      <div class="col-md-3" data-animation-effect="fadeInLeftSmall" data-effect-delay="300">
        <div class="box-style effect2">
          <div class="image-wrapper"> <img src="img/DSE_3486.jpg" alt=""> <a href="#" class="detail">Trip Detail</a></div>
          <div class="box-content">
            <h3><a href="#">Annapurna Base Camp</a></h3>
            <p><i class="fa fa-clock-o"></i> Trip Duration: <span class="blue-text">18 Day(s)</span></p>
          </div>
          <span class="price-tag orange-bg"><span>All Price Inclusive</span> $500</span> </div>
      </div>
      <div class="col-md-3" data-animation-effect="fadeInLeftSmall" data-effect-delay="400">
        <div class="box-style effect2">
          <div class="image-wrapper"> <img src="img/DSE_3486.jpg" alt=""> <a href="#" class="detail">Trip Detail</a></div>
          <div class="box-content">
            <h3><a href="#">Annapurna Base Camp</a></h3>
            <p><i class="fa fa-clock-o"></i> Trip Duration: <span class="blue-text">18 Day(s)</span></p>
          </div>
          <span class="price-tag green-bg"><span>All Price Inclusive</span> $500</span> </div>
      </div>
    </div>
  </div>
</section>



<!--Map Start-->
<!--<section id="map"> </section>-->
<!--Map End-->
<?php include('footer.php')?>