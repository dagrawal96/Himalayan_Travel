<?php include('header.php');
      require('AdminLTE/inc/config.php');
      $PackageId=$_GET['id'];
      $latPackage=$mysqli->query("select * from packages where PackageId=$PackageId");
      $SiPackage=$latPackage->fetch_array();
      $CategoryId=$SiPackage["CategoryId"];
	    $PackageId=$SiPackage["PackageId"];
	    $Title=$SiPackage["Title"];
      $Description=$SiPackage["Description"];
      $Photo=$SiPackage["Photo"];
    	$Facts=$SiPackage["Facts"];
    	$DetailItenary=$SiPackage["DetailItenary"];
    	$CostInclude=$SiPackage["CostInclude"];
    	$CostExclude=$SiPackage["CostExclude"];
    	$Notes=$SiPackage["Notes"];
    	$Cancellation=$SiPackage["Cancellation"];
?>
<!--Hero Section Start-->
<section class="page-top">
  <?php include('menu.php')?>
  <div class="container">
    <div class="page-top-content" data-animation-effect="fadeInLeftSmall" data-effect-delay="300">
      <h2 class="pull-left">Trekking Detail</h2>
      <div class="back-to-home pull-right"><a href="index.php"><i class="fa fa-home"></i> Back to home</a></div>
    </div>
  </div>
</section>
<!--Hero Section End-->
<section class="inner-content package-section" id="trekking">
  <div class="container">        
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
       <?php
       $i=0;
       $html='';
      $latPhotos=$mysqli->query("select * from photos where PackageId=$PackageId");
      while($SiPhoto=$latPhotos->fetch_array()){
         $Photor=$SiPhoto["Imagename"];
         if($i==0){
          $html.='<div class="carousel-item active">';
          $html.='<img class="d-block img-fluid" src="img/';
          $html.=$Photor;
          $html.='" alt="First slide">';
          $html.='</div>';
        }
        else {
           $html.='<div class="carousel-item">';
           $html.='<img class="d-block img-fluid" src="img/';
           $html.=$Photor;
           $html.='" alt="First slide">';
           $html.='</div>';
        }
        $i=$i+1;
      }
      ?>
      <?=$html?>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
    </div>
          
          <div class="btn-wrap clearfix"> <a href="contact.php" class="enquirebtn">Enquiry Us</a> <a href="booking-form.php" class="bookbtn">Book This Trip</a> </div>
          
          <!-- tabs start -->
          <div class="tabs-style"> 
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li  class="nav-item"><a href="#h2tab1" class="nav-link active" role="tab" data-toggle="tab">Trip Overview</a></li>
              <li class="nav-item"><a href="#h2tab2" class="nav-link" role="tab" data-toggle="tab">Detail Itenary</a></li>
              <li class="nav-item"><a href="#h2tab3" class="nav-link" role="tab" data-toggle="tab">Inclusions & Exclusions</a></li>
              <li class="nav-item"><a href="#h2tab4" class="nav-link" role="tab" data-toggle="tab">Trip Facts</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
              <div class="tab-pane fade in active" id="h2tab1">
                <p><?=$Description?></p>
                
              </div>
              <div class="tab-pane fade" id="h2tab2">
                <?=$DetailItenary?>
              </div>
              <div class="tab-pane fade" id="h2tab3"> <strong>The Trip cost will be vary depending on the Group size duration of days and Services required please contact us Via our email <a href="#">thimalayanepal@gmail.com</a> with your details to obtain a quote.</strong>
                <div class="row">
                  <div class="col-md-6">
                    <h3>Includes:</h3>
                   <?=$CostInclude?>
                  </div>
                  <div class="col-md-6">
                    <h3>Excludes:</h3>
                   <?=$CostExclude?>
                  </div>
                </div>
                </div>
                <div class="tab-pane fade" id="h2tab4">
             
                  <?=$Facts?>
								  
                
                </div>
            </div>
          </div>
          <!-- tabs end --> 
       
  </div>
</section>

<!--Map Start--><!-- 
<section id="map"> </section>
 --><!--Map End-->
<?php include('footer.php')?>